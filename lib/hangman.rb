class Hangman
  attr_reader :guesser, :referee, :board, :word

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guessed_letter = @guesser.guess(board)
    indices = @referee.check_guess(guessed_letter)
    unless indices.empty?
      update_board(indices, guessed_letter)
    end
    @guesser.handle_response
  end

  def update_board(indices, letter)
    indices.each do |idx|
      @board[idx] = letter
    end
  end
end




class HumanPlayer

end




class ComputerPlayer

  attr_reader :word, :name, :candidate_words, :length

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = @dictionary
  end

  def update_list
    @candidate_words.delete_if { |word| word.chars.length != @length }
  end

  def guess(board)
    letters = ('a'..'z').to_a
    guessed_letter = letters.sample
    # board.include?(guessed_letter) ? guess(board) : guessed_letter
    most_common_letter(board)
  end

  def most_common_letter(board)
    array = @candidate_words.join.split("")
    freq = array.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
    board.each do |letter|
      freq.delete_if { |key| board.include?(key) }
    end
    array.max_by { |v| freq[v] }
  end

  def handle_response(letter, array = [0])
    @candidate_words.each do |word|
      array.each do |idx|
        word[idx] == letter ? nil : @candidate_words.delete(word)
      end
      word.chars.count(letter) == array.count ? nil : @candidate_words.delete(word)
    end
    @candidate_words
  end



  def register_secret_length(length)
    @length = length
    # puts "Length of word: #{length} characters"
    update_list
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def check_guess(letter)
    indices = []
    @word.chars.each_index do |idx|
      @word[idx] == letter ? indices << idx : nil
    end
    indices
  end
end
